package com.example.springredis.config;

import com.example.springredis.model.*;
import com.example.springredis.repository.*;
import com.example.springredis.service.CartService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.redislabs.lettusearch.*;
import io.lettuce.core.RedisCommandExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.stream.StreamSupport;

import static org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair.fromSerializer;

@Configuration
@EnableCaching
@Slf4j
public class DataConfig {

	@Bean
	public RedisCacheManager cacheManager(RedisConnectionFactory conFactory) {
		var cacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
										 .prefixCacheNameWith(this.getClass().getName() + ".cache" + ".")
										 .entryTtl(Duration.ofHours(1))
										 .disableCachingNullValues()
										 .serializeValuesWith(fromSerializer(new GenericJackson2JsonRedisSerializer()));

		return RedisCacheManager.builder(conFactory)
					   .cacheDefaults(cacheConfiguration)
					   .build();
	}

	@Bean
	public RedisTemplate<?, ?> redisTemplate(RedisConnectionFactory conFactory) {
		var redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(conFactory);

		return redisTemplate;
	}

	@Bean
	@Order(1)
	public CommandLineRunner loadRoles(RoleRepository roleRepository) {
		return (args) -> {
			if (roleRepository.count() == 0) {
				var roleAdmin = Role.builder().name("admin").build();
				var roleCustomer = Role.builder().name("customer").build();

				roleRepository.save(roleAdmin);
				roleRepository.save(roleCustomer);

				log.info("Admin and Customer roles are created.");
			}
		};
	}

	@Bean
	@Order(2)
	public CommandLineRunner loadUsers(
			UserRepository userRepository,
			RoleRepository roleRepository,
			PasswordEncoder passwordEncoder,
			ObjectMapper objectMapper
	) {

		return (args) -> {
			if (userRepository.count() == 0) {

				var roleAdmin = roleRepository.findFirstByName("admin").orElseThrow();
				var roleCustomer = roleRepository.findFirstByName("customer").orElseThrow();

				try {
					var inputStream = getClass().getResourceAsStream("/data/users/users.json");
					var userListTypeRef = new TypeReference<List<User>>() {
					};
					var userList = objectMapper.readValue(inputStream, userListTypeRef);

					userList.forEach(user -> {
						user.setPassword(passwordEncoder.encode(user.getPassword()));
						user.addRole(roleCustomer);
						userRepository.save(user);
					});
					log.info("Users imported! Size: {}", userRepository.count());
				} catch (Exception e) {
					log.error("unable to import users.", e);
				}

				var userAdmin = User.builder().name("AMD").email("amd@gmail.com")
										.password(passwordEncoder.encode("123456"))
										.roles(Set.of(roleAdmin)).build();
				userRepository.save(userAdmin);
				log.info("Admin User imported!");

			}
		};
	}

	@Bean
	@Order(3)
	public CommandLineRunner loadBooks(
			BookRepository bookRepository,
			CategoryRepository categoryRepository,
			ObjectMapper objectMapper
	) {
		return (args) -> {

			if (bookRepository.count() != 0) return;

			var savedCategories = new HashMap<String, Category>();
			var listBookTypeRef = new TypeReference<List<Book>>() {
			};

			try {
				var srcPath = Paths.get(getClass().getResource("/data/books/").toURI());
				var pathList = Files.list(srcPath)
									   .filter(Files::isRegularFile)
									   .filter(path -> path.getFileName().toString().endsWith(".json"))
									   .toList();

				for (var path : pathList) {
					var fileName = path.getFileName().toString();
					var categoryName = fileName.substring(0, fileName.indexOf("_"));
					var category = savedCategories.computeIfAbsent(
							categoryName,
							categoryNameKey -> {
								var newCategory = Category.builder().name(categoryNameKey).build();
								return categoryRepository.save(newCategory);
							}
					);

					log.info("Reading {} category...", categoryName);

					var bookList = objectMapper.readValue(Files.newInputStream(path), listBookTypeRef);
					bookList.forEach(book -> {
						book.addCategory(category);
						bookRepository.save(book);
					});

					log.info("{} category imported. Size: {}", categoryName, bookList.size());
				}

				log.info("All books imported! Size: {}", bookRepository.count());
			} catch (Exception e) {
				log.error("unable to import books.", e);
			}

		};
	}

	@Bean
	@Order(4)
	public CommandLineRunner loadRandomBookRating(
			BookRatingRepository bookRatingRepository,
			UserRepository userRepository,
			BookRepository bookRepository,
			RedisTemplate<String, String> redisTemplate,
			@Value("${app.numberOfRating}") Integer numberOfRating
	) {
		return (args) -> {

			if (bookRatingRepository.count() != 0) return;

			for (int i = 0; i < numberOfRating; i++) {

				var bookId = redisTemplate.opsForSet().randomMember(Book.class.getName());
				var userId = redisTemplate.opsForSet().randomMember(User.class.getName());

				var book = bookRepository.findById(bookId).orElseThrow();
				var user = userRepository.findById(userId).orElseThrow();
				var rate = new Random().nextInt(3, 10);

				var bookRating = BookRating.builder()
										 .book(book)
										 .user(user)
										 .rating(rate)
										 .build();

				bookRatingRepository.save(bookRating);
			}

			log.info("Book Rating imported! Size: {}", numberOfRating);
		};
	}

	@Bean
	@Order(5)
	public CommandLineRunner loadSampleCats(
			RedisTemplate<String, String> redisTemplate,
			CartRepository cartRepository,
			BookRepository bookRepository,
			CartService cartService,
			@Value("${app.numberOfCarts}") Integer numberOfCarts
	) {
		return (args) -> {

			if (cartRepository.count() != 0) return;

			var random = new Random();

			for (int i = 0; i < numberOfCarts; i++) {

				var userId = redisTemplate.opsForSet().randomMember(User.class.getName());
				var cart = Cart.builder().userId(userId).build();
				var bookSet = getRandomBooks(bookRepository, redisTemplate, 7);
				cart.setCartItems(getCartItemsForBooks(bookSet));
				cartRepository.save(cart);

				if (random.nextBoolean()) {
					cartService.checkout(cart.getId());
				}
			}

			log.info("Created Carts: Size {}", cartRepository.count());
		};
	}

	private Set<Book> getRandomBooks(BookRepository bookRepository,
									 RedisTemplate<String, String> redisTemplate,
									 int max) {

		var random = new Random();
		int howMany = random.nextInt(max) + 1;
		var bookSet = new HashSet<Book>();
		for (int i = 0; i < howMany; i++) {
			var randomBookId = redisTemplate.opsForSet().randomMember(Book.class.getName());
			bookSet.add(bookRepository.findById(randomBookId).get());
		}
		return bookSet;
	}

	private Set<CartItem> getCartItemsForBooks(Set<Book> books) {

		var itemSet = new HashSet<CartItem>();
		books.forEach(book -> {
			var item = CartItem.builder()
							   .isbn(book.getId())
							   .price(book.getPrice())
							   .quantity(1L)
							   .build();
			itemSet.add(item);
		});

		return itemSet;
	}

	@Bean
	@Order(6)
	@SuppressWarnings("unchecked")
	public CommandLineRunner createBookSearchIndex(
			StatefulRediSearchConnection<String, String> searchConnection,
			@Value("${app.booksSearchIndexName}") String searchIndexName
	) {
		return args -> {

			RediSearchCommands<String, String> commands = searchConnection.sync();

			try {
				commands.ftInfo(searchIndexName);
			} catch (RedisCommandExecutionException ex) {

				if (ex.getMessage().equals("Unknown Index name")) {

					var options = CreateOptions.<String, String>builder()
										  .prefix(Book.class.getName() + ":")
										  .build();
					var title = Field.text("title").sortable(true).build();
					var subtitle = Field.text("subtitle").build();
					var description = Field.text("description").build();
					var author0 = Field.text(" authors.[0]").build();
					var author1 = Field.text(" authors.[1]").build();
					var author2 = Field.text(" authors.[2]").build();
					var author3 = Field.text(" authors.[3]").build();
					var author4 = Field.text(" authors.[4]").build();
					var author5 = Field.text(" authors.[5]").build();
					var author6 = Field.text(" authors.[6]").build();

					commands.create(
							searchIndexName,
							options,
							title, subtitle, description,
							author0, author1, author2, author3, author4, author5, author6
					);

					log.info("Created Books Search Index...");
				}

			}

		};
	}

	@Bean
	@Order(7)
	public CommandLineRunner createAuthorNameSuggestions(
			RedisTemplate<String, String> redisTemplate,
			BookRepository bookRepository,
			StatefulRediSearchConnection<String, String> searchConnection,
			@Value("${app.autoCompleteKey}") String autoCompleteKey
	) {
		return (args) -> {

			if (redisTemplate.hasKey("autoCompleteKey")) return;

			var commands = searchConnection.sync();

			StreamSupport.stream(bookRepository.findAll().spliterator(), false)
					.filter(book -> book.getAuthors() != null && !book.getAuthors().isEmpty())
					.flatMap(book -> book.getAuthors().stream())
					.map(author -> Suggestion.builder(author).build())
					.forEach(authorSuggest -> commands.sugadd(autoCompleteKey, authorSuggest));

			log.info("Created Author Name Suggestions...");
		};
	}

}
