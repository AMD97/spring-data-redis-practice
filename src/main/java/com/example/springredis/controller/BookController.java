package com.example.springredis.controller;

import com.example.springredis.model.Book;
import com.example.springredis.model.Category;
import com.example.springredis.model.PageBookDto;
import com.example.springredis.repository.BookRepository;
import com.example.springredis.repository.CategoryRepository;
import com.redislabs.lettusearch.SearchResults;
import com.redislabs.lettusearch.StatefulRediSearchConnection;
import com.redislabs.lettusearch.Suggestion;
import com.redislabs.lettusearch.SuggetOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {

	private final BookRepository bookRepository;
	private final CategoryRepository categoryRepository;
	private final StatefulRediSearchConnection<String, String> searchConnection;

	@Value("${app.booksSearchIndexName}")
	private String searchIndexName;

	@Value("${app.autoCompleteKey}")
	private String autoCompleteKey;

	@GetMapping
	public ResponseEntity<PageBookDto> getAll(
			@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size
	) {
		var pageRequest = PageRequest.of(page, size);
		var bookPage = bookRepository.findAll(pageRequest);
		if (bookPage.hasContent()) {
			return ResponseEntity.ok(
					PageBookDto.builder()
							.books(bookPage.getContent())
							.page(bookPage.getNumber())
							.totalPage(bookPage.getTotalPages())
							.totalElement(bookPage.getTotalElements())
							.build()
			);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/{isbn}")
	@Cacheable(cacheNames = "books",key = "#{isbn}")
	public ResponseEntity<Book> getByIsbn(@PathVariable String isbn) {
		return bookRepository.findById(isbn)
					   .map(ResponseEntity::ok)
					   .orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping("/categories")
	@ResponseStatus(HttpStatus.OK)
	public Iterable<Category> getCategories() {
		return categoryRepository.findAll();
	}

	@GetMapping("/search")
	public SearchResults<String, String> search(@RequestParam(name = "q") String query) {
		var commands = searchConnection.sync();
		return commands.search(searchIndexName, query);
	}

	@GetMapping("/authors")
	public List<Suggestion<String>> authorAutoComplete(@RequestParam(name = "q") String query) {
		var commands = searchConnection.sync();
		var options = SuggetOptions.builder().max(20L).build();
		return commands.sugget(autoCompleteKey, query, options);
	}
}
