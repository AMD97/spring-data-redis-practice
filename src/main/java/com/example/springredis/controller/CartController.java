package com.example.springredis.controller;

import com.example.springredis.model.Cart;
import com.example.springredis.model.CartItem;
import com.example.springredis.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/carts")
@RequiredArgsConstructor
public class CartController {

	private final CartService cartService;

	@GetMapping("/{id}")
	public Cart get(@PathVariable("id") String id) {
		return cartService.get(id);
	}

	@PostMapping("/{id}")
	public void addToCart(@PathVariable("id") String id, @RequestBody CartItem item) {
		cartService.addToCart(id, item);
	}

	@DeleteMapping("/{id}")
	public void removeFromCart(@PathVariable("id") String id, @RequestBody String isbn) {
		cartService.removeFromCart(id, isbn);
	}

	@PostMapping("/{id}/checkout")
	public void checkout(@PathVariable("id") String id) {
		cartService.checkout(id);
	}

}

