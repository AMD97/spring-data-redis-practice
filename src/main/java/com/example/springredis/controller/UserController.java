package com.example.springredis.controller;

import com.example.springredis.model.User;
import com.example.springredis.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

	private final UserRepository userRepository;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Iterable<User> getAll() {
		return userRepository.findAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getById(@PathVariable String id) {
		return userRepository.findById(id)
					   .map(ResponseEntity::ok)
					   .orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping("/email/{email}")
	public ResponseEntity<User> getByEmail(
			@PathVariable String email
	) {
		return userRepository.findFirstByEmail(email)
					   .map(ResponseEntity::ok)
					   .orElseGet(() -> ResponseEntity.notFound().build());
	}
}
