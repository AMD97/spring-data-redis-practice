package com.example.springredis;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/redis/")
@RequiredArgsConstructor
public class Controller {

	private final RedisTemplate<String, String> redisTemplate;
	private static final String STRING_KEY_PREFIX = "redis2read:strings:";

	@PostMapping("/string")
	public ResponseEntity<?> create(@RequestBody Map.Entry<String, String> keyVal) {

		redisTemplate.opsForValue()
				.set(STRING_KEY_PREFIX + keyVal.getKey(), keyVal.getValue());

		return ResponseEntity.created(URI.create("/" + keyVal.getKey()))
					   .build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<String> get(@PathVariable String id) {
		var valueOpt = Optional.ofNullable(
				redisTemplate.opsForValue()
						.get(STRING_KEY_PREFIX + id)
		);
		return valueOpt.map(ResponseEntity::ok)
					   .orElseGet(() -> ResponseEntity.notFound().build());
	}

}
