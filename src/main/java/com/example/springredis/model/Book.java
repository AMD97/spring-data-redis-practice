package com.example.springredis.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.redis.core.RedisHash;

import java.util.HashSet;
import java.util.Set;

@RedisHash
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
public class Book {

	@Id
	@EqualsAndHashCode.Include
	private String id;
	private String title;
	private String subtitle;
	private String description;
	private String language;
	private Long pageCount;
	private String thumbnail;
	private Double price;
	private String currency;
	private String infoLink;
	private Set<String> authors;
	@Reference
	private Set<Category> categories = new HashSet<>();

	public void addCategory(Category category) {
		categories.add(category);
	}

}
