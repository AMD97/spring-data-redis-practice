package com.example.springredis.model;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class PageBookDto {

	private Collection<Book> books;
	private Integer page;
	private Integer totalPage;
	private Long totalElement;

}
