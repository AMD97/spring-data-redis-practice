package com.example.springredis.model;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.HashSet;
import java.util.Set;

@Data
@Builder
public class Cart {

	private String id;
	private String userId;

	@Singular
	private Set<CartItem> cartItems = new HashSet<>();

	public int count() {
		return getCartItems().size();
	}

	public Double getTotal() {
		return cartItems.stream()
					   .mapToDouble(item -> item.getPrice() * item.getQuantity())
					   .sum();
	}

}
