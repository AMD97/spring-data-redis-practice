package com.example.springredis.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Category {

	@Id
	private String id;
	private String name;

}
