package com.example.springredis.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartItem {

	public String isbn;
	public Double price;
	public Long quantity;

}
