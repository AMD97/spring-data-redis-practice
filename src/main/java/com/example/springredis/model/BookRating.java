package com.example.springredis.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.redis.core.RedisHash;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@RedisHash
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
public class BookRating {

	@Id
	@EqualsAndHashCode.Include
	private String id;

	@NotNull
	@Reference
	private User user;

	@NotNull
	@Reference
	private Book book;

	@NotNull
	private Integer rating;

}
