package com.example.springredis.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.annotation.Transient;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@RedisHash
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(value = {"password", "passwordConfirm"}, allowSetters = true)
public class User {
	@Id
	private String id;

	@NonNull
	@Size(min = 2, max = 48)
	private String name;

	@NonNull
	@Email
	@Indexed
	private String email;

	@NonNull
	private String password;

	@Transient
	private String passwordConfirm;

	@Reference
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Role> roles = new HashSet<>();

	@Reference
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Book> books = new HashSet<>();

	public void addBook(Book book) {
		books.add(book);
	}

	public void addRole(Role role) {roles.add(role);
	}

	@Override
	public String toString() {
		return "User{" +
			   "id='" + id + '\'' +
			   ", name='" + name + '\'' +
			   ", email='" + email + '\'' +
			   '}';
	}
}
