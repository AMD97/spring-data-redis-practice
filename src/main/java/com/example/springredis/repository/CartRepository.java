package com.example.springredis.repository;

import com.example.springredis.model.Cart;
import com.redislabs.modules.rejson.JReJSON;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

@Repository
@RequiredArgsConstructor
public class CartRepository implements CrudRepository<Cart, String> {

	private final JReJSON redisJson = new JReJSON();
	private final static String ID_PREFIX = Cart.class.getName();

	private final RedisTemplate<String, String> template;

	private SetOperations<String, String> redisSets() {
		return template.opsForSet();
	}

	private HashOperations<String, String, String> redisHash() {
		return template.opsForHash();
	}

	public static String generateKey(Cart cart) {
		return generateKey(ID_PREFIX, cart.getId());
	}

	public static String generateKey(String cartId) {
		return generateKey(ID_PREFIX, cartId);
	}

	private static String generateKey(String prefix, String cartId) {
		return String.format("%s:%s", prefix, cartId);
	}

	@Override
	public <S extends Cart> S save(S cart) {

		if (cart.getId() == null) {
			cart.setId(UUID.randomUUID().toString());
		}
		var key = generateKey(cart);

		redisJson.set(key, cart);
		redisSets().add(ID_PREFIX, key);
		redisHash().put("carts-by-user-id-idx", cart.getUserId(), cart.getId());

		return cart;
	}

	@Override
	public <S extends Cart> Iterable<S> saveAll(Iterable<S> carts) {
		return StreamSupport.stream(carts.spliterator(), false)
					   .map(this::save)
					   .toList();
	}

	@Override
	public Optional<Cart> findById(String id) {
		return Optional.ofNullable(
				redisJson.get(generateKey(id), Cart.class)
		);
	}

	@Override
	public boolean existsById(String id) {
		return template.hasKey(generateKey(id));
	}

	@Override
	public Iterable<Cart> findAll() {

		var keySet = template.opsForSet().members(ID_PREFIX);

		if (keySet == null || keySet.isEmpty())
			return new ArrayList<>(0);

		return redisJson.mget(Cart.class, keySet.toArray(String[]::new));
	}

	@Override
	public Iterable<Cart> findAllById(Iterable<String> ids) {

		var keyArr = StreamSupport.stream(ids.spliterator(), false)
							 .map(CartRepository::generateKey)
							 .toArray(String[]::new);

		return redisJson.mget(Cart.class, keyArr);
	}

	@Override
	public long count() {
		return redisSets().size(ID_PREFIX);
	}

	@Override
	public void deleteById(String id) {
		redisJson.del(generateKey(id));
	}

	@Override
	public void delete(Cart cart) {
		deleteById(cart.getId());
	}

	@Override
	public void deleteAllById(Iterable<? extends String> ids) {

		var keyList = StreamSupport.stream(ids.spliterator(), false)
							  .map(CartRepository::generateKey)
							  .toList();

		redisSets().getOperations().delete(keyList);
	}

	@Override
	public void deleteAll(Iterable<? extends Cart> carts) {

		var keyList = StreamSupport.stream(carts.spliterator(), false)
							  .map(Cart::getId)
							  .map(CartRepository::generateKey)
							  .toList();

		redisSets().getOperations().delete(keyList);
	}

	@Override
	public void deleteAll() {
		redisSets().getOperations().delete(redisSets().members(ID_PREFIX));
	}
}
