package com.example.springredis.repository;

import com.example.springredis.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, String> {

	Optional<Role> findFirstByName(String name);

}
