package com.example.springredis.service;

import com.example.springredis.model.Cart;
import com.example.springredis.model.CartItem;
import com.example.springredis.repository.BookRepository;
import com.example.springredis.repository.CartRepository;
import com.example.springredis.repository.UserRepository;
import com.redislabs.modules.rejson.JReJSON;
import com.redislabs.modules.rejson.Path;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.LongStream;

@Service
@RequiredArgsConstructor
public class CartService {

	private final CartRepository cartRepository;
	private final BookRepository bookRepository;
	private final UserRepository userRepository;

	private final JReJSON redisJson = new JReJSON();

	Path cartItemsPath = Path.of(".cartItems");

	public Cart get(String id) {
		return cartRepository.findById(id)
					   .orElseThrow(() -> new IllegalArgumentException("id not exist"));
	}

	public void addToCart(String id, CartItem item) {
		var bookOpt = bookRepository.findById(item.getIsbn());
		if (bookOpt.isPresent()) {
			String cartKey = CartRepository.generateKey(id);
			item.setPrice(bookOpt.get().getPrice());
			redisJson.arrAppend(cartKey, cartItemsPath, item);
		}
	}

	public void removeFromCart(String id, String isbn) {
		var cartOpt = cartRepository.findById(id);
		if (cartOpt.isPresent()) {
			var cart = cartOpt.get();
			var cartKey = CartRepository.generateKey(cart.getId());
			var cartItems = new ArrayList<>(cart.getCartItems());
			var cartItemIndexOpt = LongStream.range(0, cartItems.size())
										   .filter(i -> cartItems.get((int) i).getIsbn().equals(isbn))
										   .findFirst();
			if (cartItemIndexOpt.isPresent()) {
				redisJson.arrPop(cartKey, CartItem.class, cartItemsPath, cartItemIndexOpt.getAsLong());
			}
		}
	}

	public void checkout(String id) {
		var cart = cartRepository.findById(id).get();
		var user = userRepository.findById(cart.getUserId()).get();
		cart.getCartItems()
				.forEach(cartItem -> {
					var book = bookRepository.findById(cartItem.getIsbn()).get();
					user.addBook(book);
				});
		userRepository.save(user);
		// cartRepository.delete(cart);
	}
}
